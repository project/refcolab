
CONTENTS OF THIS FILE
---------------------

 * Requirements
 * Optional requirements
 * Installation
 * Configuration
 * Theming

REQUIREMENTS
------------

Drupal requires a web server, PHP 4 (4.3.5 or greater) or PHP 5
(http://www.php.net/) and either MySQL (http://www.mysql.com/) or PostgreSQL
(http://www.postgresql.org/). The Apache web server and MySQL database are
recommended; other web server and database combinations such as IIS and
PostgreSQL have been tested to a lesser extent. When using MySQL, version 4.1.1
or greater is recommended to assure you can safely transfer the database.

For more detailed information about Drupal requirements, see "Requirements"
(http://drupal.org/requirements) in the Drupal handbook.

For detailed information on how to configure a test server environment using
a variety of operating systems and web servers, see "Local server setup"
(http://drupal.org/node/157602) in the Drupal handbook.

OPTIONAL REQUIREMENTS
---------------------

- To use XML-based services such as the Blogger API and RSS syndication,
  you will need PHP's XML extension. This extension is enabled by default.

- To use Drupal's "Clean URLs" feature on an Apache web server, you will need
  the mod_rewrite module and the ability to use local .htaccess files. For
  Clean URLs support on IIS, see "Using Clean URLs with IIS"
  (http://drupal.org/node/3854) in the Drupal handbook.

- Various Drupal features require that the web server process (for
  example, httpd) be able to initiate outbound connections. This is usually
  possible, but some hosting providers or server configurations forbid such
  connections. The features that depend on this functionality include the
  integrated "Update status" module (which downloads information about
  available updates of Drupal core and any installed contributed modules and
  themes), the ability to log in via OpenID, fetching aggregator feeds, or
  other network-dependent services.


INSTALLATION
------------

CONFIGURATION
-------------

THEMING
-------
