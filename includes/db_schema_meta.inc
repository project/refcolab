<?php

require_once './'. drupal_get_path('module', 'refcolab') .'/includes/fields_meta.inc';
require_once './'. drupal_get_path('module', 'refcolab') .'/includes/reference_types_meta.inc';

/**
 * Return the DB schema used to store the meta data for
 * reference types - fields relationship.
 */
function _refcolab_db_schema_meta() {
  $meta['refcolab_meta_types'] = array(
    'description' => t('Reference types defined in the system.'),
    'fields' => array(
      'type_name' => array(
        'description' => t('Primary Key: Unique reference type name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'type_title' => array(
        'description' => t('Reference name title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'description' => array(
        'description' => t('Reference type description.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
      ),
      'user_defined' => array(
        'description' => t('Indicates whether the reference type is define by the user.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('type_name'),
  );
  $meta['refcolab_meta_fields'] = array(
    'description' => t('Reference\'s fields.'),
    'fields' => array(
      'field_name' => array(
        'description' => t('Primary Key: Unique field name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'field_title' => array(
        'description' => t('Field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'description' => array(
        'description' => t('Field description.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
      ),
      'widget' => array(
        'description' => t('A serialized array containing the widge data for the field.'),
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
      ),
    ),
    'primary key' => array('field_name'),
  );
  $meta['refcolab_meta_creators'] = array(
    'description' => t('Reference\'s creators fields.'),
    'fields' => array(
      'creator' => array(
        'description' => t('Primary Key: Unique creator name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'creator_title' => array(
        'description' => t('Field title.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 128,
      ),
      'description' => array(
        'description' => t('Field description.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 255,
      ),
    ),
    'primary key' => array('creator'),
  );  
  $meta['refcolab_meta_types_fields'] = array(
    'description' => t('Reference types\' fields. Stores the reference types - fields relationship'),
    'fields' => array(
      'type_name' => array(
        'description' => t('Primary Key: Unique reference type name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'field_name' => array(
        'description' => t('Primary key: Unique field name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'weight' => array(
        'description' => t('The weight of this field in relation to other fields.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
      'status' => array(
        'description' => t('Status of the relation, for example, visible or invisible.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'primary key' => array('type_name', 'field_name'),
  );
  $meta['refcolab_meta_types_creators'] = array(
    'description' => t('Reference types\' creator fields.'),
    'fields' => array(
      'type_name' => array(
        'description' => t('Primary Key: Unique reference type name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'creator' => array(
        'description' => t('Primary key: The creator field name.'),
        'type' => 'varchar',
        'not null' => TRUE,
        'length' => 32,
      ),
      'weight' => array(
        'description' => t('The weight of this field in relation to other fields.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
    ),
    'primary key' => array('type_name', 'creator'),
  );
  return $meta;
}

/**
 * Return base for all reference types tables.
 */
function _refcolab_type_table($type_name) {
  return array(
    'refcolab_type_'. $type_name => array(
      'description' => t('Stores references of type '. $type_name),
      'fields' => array(
        'nid' => array(
          'description' => t('The {node}.nid of the related node.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'vid' => array(
          'description' => t('Primary Key: The {node}.vid of the related node.'),
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
      ),
      'primary key' => array('vid'),
    ),
  );
}

/**
 * Dynamically generate the DB schema for each reference type table.
 */
function _refcolab_db_schema_dynamic() {
  $types = refcolab_reference_types_meta();
  $fields = refcolab_fields_meta();
  $schema = array();

  foreach($types as $type_name => $type) {
    $type_name = strtolower($type_name);

    $schema += _refcolab_type_table($type_name);

    foreach($type['fields'] as $field_name) {
      $schema['refcolab_type_'. $type_name]['fields'][strtolower($field_name)] = $fields[$field_name]['data'];
    }
  }

  return $schema;
}

/**
 * Generate the initial meta-data.
 */
function _refcolab_install_meta() {
  // Populate the {refcolab_meta_fields} table.
  $fields = refcolab_fields_meta();
  foreach($fields as $field_name => $field) {
    $widget = serialize($field['widget']);
    db_query("INSERT INTO {refcolab_meta_fields} (field_name, field_title, description, widget) VALUES ('%s', '%s', '%s', '%s')",
      strtolower($field_name), $field['title'], $field['description'], $widget);
  }
  
  // Populate the {refcolab_meta_creators} table.
  $creators = refcolab_creators_meta();
  foreach($creators as $creator_name  => $creator) {
    // @TODO: add description to creators.
    db_query("INSERT INTO {refcolab_meta_creators} (creator, creator_title) VALUES ('%s', '%s')",
      strtolower($creator_name), $creator['title']);
  }  

  // Populate the {refcolab_meta_types} table.
  $types = refcolab_reference_types_meta();
  foreach($types as $type_name => $type) {
    db_query("INSERT INTO {refcolab_meta_types} (type_name, type_title, description) VALUES ('%s', '%s', '%s')",
      strtolower($type_name), $type['title'], $type['description']);

    // Generate the reference type - fields relationship.
    $weight = 0;
    foreach($type['fields'] as $field_name) {
      db_query("INSERT INTO {refcolab_meta_types_fields} (type_name, field_name, weight) VALUES ('%s', '%s', %d)", strtolower($type_name), strtolower($field_name), $weight++);
    }

    // Generate the reference type - creator fields relationship.
    $weight = 0;
    foreach($type['creators'] as $creator) {
      db_query("INSERT INTO {refcolab_meta_types_creators} (type_name, creator, weight) VALUES ('%s', '%s', %d)", strtolower($type_name), strtolower($creator), $weight++);
    }
  }
}
