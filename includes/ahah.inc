<?php

/**
 * @file ahah.inc
 *
 * Handles the server side AHAH interactions.
 */


/**
 * Menu callback for AHAH creators form elements additions.
 */
function refcolab_js_more_authors() {
  // Build the new form.
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  
  // Retreive the cached form from the cache.
  $form = form_get_cache($form_build_id, $form_state);

  // Build our new form element.
  $type_name = $form['reference_type']['#value'];
  $delta = count(element_children($form['reference']['creators']));
  $form_element = _refcolab_creator_element($type_name);  

  // Add the new element to the stored form. Without adding the element to the
  // form, Drupal is not aware of this new elements existence and will not
  // process it.   
  $form['reference']['creators'][$delta] = $form_element;

  // Re-save the form in the cache with the new element added.
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  // Rebuild the form.
  $form = form_builder('reference_node_form', $form, $form_state);

  // Render the output, containing only the creatorselement added.
  $creators_form = $form['reference']['creators'];
  // Prevent duplicate wrappers.
  unset($creators_form['#prefix'], $creators_form['#suffix']); 
  $output = theme('status_messages') . drupal_render($creators_form);

  drupal_json(array('status' => TRUE, 'data' => $output));
}