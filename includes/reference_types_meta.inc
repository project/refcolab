<?php

function refcolab_reference_types_meta() {
  return array(
    'book' => array(
      'title' => t('Book'),
      'description' => t('Book'),
      'fields' => array(
        'abstractNote', 'series', 'seriesNumber', 'volume', 'numberOfVolumes', 'edition', 'place', 'publisher', 'date', 'pages', 'language', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

    'bookSection' => array(
      'title' => t('Book Section'),
      'description' => t('Book Section'),
      'fields' => array(
        'abstractNote', 'bookTitle', 'series', 'seriesNumber', 'volume', 'numberOfVolumes', 'edition', 'place', 'publisher', 'date', 'pages', 'language', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

    'journalArticle' => array(
      'title' => t('Journal Article'),
      'description' => t('Journal Article'),
      'fields' => array(
        'abstractNote', 'publicationTitle', 'volume', 'issue', 'pages', 'date', 'series', 'seriesTitle', 'seriesText', 'journalAbbreviation', 'language', 'DOI', 'ISSN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'reviewedAuthor',
      ),
    ),

    'magazineArticle' => array(
      'title' => t('Magazine Article'),
      'description' => t('Magazine Article'),
      'fields' => array(
        'abstractNote', 'publicationTitle', 'volume', 'issue', 'date', 'pages', 'language', 'ISSN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'reviewedAuthor',
      ),
    ),

    'newspaperArticle' => array(
      'title' => t('Newspaper Article'),
      'description' => t('Newspaper Article'),
      'fields' => array(
        'abstractNote', 'publicationTitle', 'edition', 'date', 'section', 'pages', 'language', 'shortTitle', 'ISSN', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'reviewedAuthor',
      ),
    ),

    'thesis' => array(
      'title' => t('Thesis'),
      'description' => t('Thesis'),
      'fields' => array(
        'abstractNote', 'thesisType', 'university', 'date', 'pages', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'tutor',
      ),
    ),

    'letter' => array(
      'title' => t('Letter'),
      'description' => t('Letter'),
      'fields' => array(
        'abstractNote', 'letterType', 'date', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'recipient',
      ),
    ),

    'manuscript' => array(
      'title' => t('Manuscript'),
      'description' => t('Manuscript'),
      'fields' => array(
        'abstractNote', 'manuscriptType', 'place', 'date', 'pages', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'translator',
      ),
    ),

    'interview' => array(
      'title' => t('Interview'),
      'description' => t('Interview'),
      'fields' => array(
        'abstractNote', 'date', 'interviewMedium', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'interviewee', 'contributor', 'translator', 'interviewer',
      ),
    ),

    'film' => array(
      'title' => t('Film'),
      'description' => t('Film'),
      'fields' => array(
        'abstractNote', 'distributor', 'videoRecordingType', 'date', 'runningTime', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'director', 'contributor', 'scriptwriter', 'producer',
      ),
    ),

    'artwork' => array(
      'title' => t('Artwork'),
      'description' => t('Artwork'),
      'fields' => array(
        'abstractNote', 'artworkMedium', 'artworkSize', 'date', 'language', 'shortTitle', 'callNumber', 'archiveLocation', 'repository', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'artist', 'contributor',
      ),
    ),

    'webpage' => array(
      'title' => t('Web Page'),
      'description' => t('Web Page'),
      'fields' => array(
        'abstractNote', 'websiteTitle', 'websiteType', 'date', 'shortTitle', 'url', 'accessDate', 'language', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'translator',
      ),
    ),

    'attachment' => array(
      'title' => t('Attachment'),
      'description' => t('Attachment'),
      'fields' => array(
        'url', 'accessDate',
      ),
      'creators' => array(
        'author', 'contributor',
      ),      
    ),

    'report' => array(
      'title' => t('Report'),
      'description' => t('Report'),
      'fields' => array(
        'abstractNote', 'reportNumber', 'reportType', 'seriesTitle', 'place', 'institution', 'date', 'pages', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

    'bill' => array(
      'title' => t('Bill'),
      'description' => t('Bill'),
      'fields' => array(
        'abstractNote', 'billNumber', 'code', 'codeVolume', 'section', 'codePages', 'legislativeBody', 'session', 'history', 'date', 'language', 'url', 'accessDate', 'shortTitle', 'rights', 'extra',
      ),
      'creators' => array(
        'sponsor', 'contributor',
      ),
    ),

    'case' => array(
      'title' => t('Case'),
      'description' => t('Case'),
      'fields' => array(
        'caseName', 'abstractNote', 'reporter', 'reporterVolume', 'court', 'firstPage', 'history', 'dateDecided', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'counsel', 'contributor',
      ),
    ),

    'hearing' => array(
      'title' => t('Hearing'),
      'description' => t('Hearing'),
      'fields' => array(
        'abstractNote', 'committee', 'place', 'publisher', 'numberOfVolumes', 'documentNumber', 'pages', 'legislativeBody', 'session', 'history', 'date', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'contributor',
      ),
    ),

    'patent' => array(
      'title' => t('Patent'),
      'description' => t('Patent'),
      'fields' => array(
        'abstractNote', 'place', 'assignee', 'patentNumber', 'date', 'pages', 'applicationNumber', 'priorityNumbers', 'issueDate', 'references', 'legalStatus', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'inventor', 'contributor', 'attorneyAgent',
      ),
    ),

    'statute' => array(
      'title' => t('Statute'),
      'description' => t('Statute'),
      'fields' => array(
        'nameOfAct', 'abstractNote', 'code', 'codeNumber', 'publicLawNumber', 'dateEnacted', 'pages', 'section', 'session', 'history', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor',
      ),
    ),

    'email' => array(
      'title' => t('E-mail'),
      'description' => t('E-mail'),
      'fields' => array(
        'subject', 'abstractNote', 'date', 'shortTitle', 'url', 'accessDate', 'language', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'recipient',
      ),
    ),

    'map' => array(
      'title' => t('Map'),
      'description' => t('Map'),
      'fields' => array(
        'abstractNote', 'mapType', 'scale', 'seriesTitle', 'edition', 'place', 'publisher', 'date', 'language', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'cartographer', 'contributor', 'seriesEditor',
      ),
    ),

    'blogPost' => array(
      'title' => t('Blog Post'),
      'description' => t('Blog Post'),
      'fields' => array(
        'abstractNote', 'blogTitle', 'websiteType', 'date', 'url', 'accessDate', 'language', 'shortTitle', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'commenter',
      ),
    ),

    'instantMessage' => array(
      'title' => t('Instant Message'),
      'description' => t('Instant Message'),
      'fields' => array(
        'abstractNote', 'date', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'recipient',
      ),
    ),

    'forumPost' => array(
      'title' => t('Forum Post'),
      'description' => t('Forum Post'),
      'fields' => array(
        'abstractNote', 'forumTitle', 'postType', 'date', 'language', 'shortTitle', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor',
      ),
    ),

    'audioRecording' => array(
      'title' => t('Audio Recording'),
      'description' => t('Audio Recording'),
      'fields' => array(
        'abstractNote', 'audioRecordingType', 'seriesTitle', 'volume', 'numberOfVolumes', 'place', 'label', 'date', 'runningTime', 'language', 'ISBN', 'shortTitle', 'callNumber', 'archiveLocation', 'repository', 'url', 'accessDate', 'rights', 'extra',
      ),
      'creators' => array(
        'performer', 'contributor', 'composer', 'wordsBy',
      ),
    ),

    'presentation' => array(
      'title' => t('Presentation'),
      'description' => t('Presentation'),
      'fields' => array(
        'abstractNote', 'presentationType', 'date', 'place', 'meetingName', 'url', 'accessDate', 'language', 'shortTitle', 'rights', 'extra',
      ),
      'creators' => array(
        'presenter', 'contributor',
      ),
    ),

    'videoRecording' => array(
      'title' => t('Video Recording'),
      'description' => t('Video Recording'),
      'fields' => array(
        'abstractNote', 'videoRecordingType', 'seriesTitle', 'volume', 'numberOfVolumes', 'place', 'studio', 'date', 'runningTime', 'language', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'director', 'contributor', 'scriptwriter', 'producer', 'castMember',
      ),
    ),

    'tvBroadcast' => array(
      'title' => t('TV Broadcast'),
      'description' => t('TV Broadcast'),
      'fields' => array(
        'abstractNote', 'seriesTitle', 'episodeNumber', 'videoRecordingType', 'place', 'network', 'date', 'runningTime', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'director', 'contributor', 'scriptwriter', 'producer', 'castMember', 'guest',
      ),
    ),

    'radioBroadcast' => array(
      'title' => t('Radio Broadcast'),
      'description' => t('Radio Broadcast'),
      'fields' => array(
        'abstractNote', 'seriesTitle', 'episodeNumber', 'audioRecordingType', 'place', 'network', 'date', 'runningTime', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'director', 'contributor', 'scriptwriter', 'producer', 'castMember', 'guest',
      ),
    ),

    'podcast' => array(
      'title' => t('Podcast'),
      'description' => t('Podcast'),
      'fields' => array(
        'abstractNote', 'seriesTitle', 'episodeNumber', 'audioFileType', 'runningTime', 'url', 'accessDate', 'language', 'shortTitle', 'rights', 'extra',
      ),
      'creators' => array(
        'podcaster', 'contributor', 'guest',
      ),
    ),

    'computerProgram' => array(
      'title' => t('Computer Program'),
      'description' => t('Computer Program'),
      'fields' => array(
        'abstractNote', 'seriesTitle', 'version', 'system', 'place', 'company', 'programmingLanguage', 'ISBN', 'shortTitle', 'url', 'rights', 'callNumber', 'archiveLocation', 'repository', 'accessDate', 'extra',
      ),
      'creators' => array(
        'programmer', 'contributor',
      ),
    ),

    'conferencePaper' => array(
      'title' => t('Conference Paper'),
      'description' => t('Conference Paper'),
      'fields' => array(
        'abstractNote', 'date', 'proceedingsTitle', 'conferenceName', 'place', 'publisher', 'volume', 'pages', 'series', 'language', 'DOI', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

    'document' => array(
      'title' => t('Document'),
      'description' => t('Document'),
      'fields' => array(
        'abstractNote', 'publisher', 'date', 'language', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'reviewedAuthor',
      ),
    ),

    'encyclopediaArticle' => array(
      'title' => t('Encyclopedia Article'),
      'description' => t('Encyclopedia Article'),
      'fields' => array(
        'abstractNote', 'encyclopediaTitle', 'series', 'seriesNumber', 'volume', 'numberOfVolumes', 'edition', 'place', 'publisher', 'date', 'pages', 'ISBN', 'shortTitle', 'url', 'accessDate', 'language', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

    'dictionaryEntry' => array(
      'title' => t('Dictionary Entry'),
      'description' => t('Dictionary Entry'),
      'fields' => array(
        'abstractNote', 'dictionaryTitle', 'series', 'seriesNumber', 'volume', 'numberOfVolumes', 'edition', 'place', 'publisher', 'date', 'pages', 'language', 'ISBN', 'shortTitle', 'url', 'accessDate', 'callNumber', 'archiveLocation', 'repository', 'rights', 'extra',
      ),
      'creators' => array(
        'author', 'contributor', 'editor', 'translator', 'seriesEditor',
      ),
    ),

  );
}
