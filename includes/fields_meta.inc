<?php

function refcolab_fields_meta() {
  return array(
    'abstractNote' => array(
      'title' => t('Abstract'),
      'description' => t('Abstract'),
      'widget' => array(
        '#type' => 'textarea',
        '#rows' => 5,
       ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
       ),
    ),

    'series' => array(
      'title' => t('Series'),
      'description' => t('Series'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'seriesNumber' => array(
      'title' => t('Series Number'),
      'description' => t('Series Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 20,
       ),
    ),

    'volume' => array(
      'title' => t('Volume'),
      'description' => t('Volume'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 20,
       ),
    ),

    'numberOfVolumes' => array(
      'title' => t('# of Volumes'),
      'description' => t('# of Volumes'),
      'widget' => array(
        '#type' => 'textfield',
        '#size' => 5,
        '#maxlength' => 5,
       ),
      'data' => array(
        'type' => 'int',
        'not null' => FALSE,
        'size' => 'small',
       ),
    ),

    'edition' => array(
      'title' => t('Edition'),
      'description' => t('Edition'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'place' => array(
      'title' => t('Place'),
      'description' => t('Place'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'publisher' => array(
      'title' => t('Publisher'),
      'description' => t('Publisher'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'date' => array(
      'title' => t('Date'),
      'description' => t('Date'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'pages' => array(
      'title' => t('Pages'),
      'description' => t('Pages'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 20,
        '#size' => 20,
      ),
      'data' => array(
        'type' => 'int',
        'not null' => FALSE,
       ),
    ),

    'language' => array(
      'title' => t('Language'),
      'description' => t('Language'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'ISBN' => array(
      'title' => t('ISBN'),
      'description' => t('ISBN'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 24,
        '#size' => 24,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'shortTitle' => array(
      'title' => t('Short Title'),
      'description' => t('Short Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'url' => array(
      'title' => t('URL'),
      'description' => t('URL'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'accessDate' => array(
      'title' => t('Accessed'),
      'description' => t('Accessed'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'callNumber' => array(
      'title' => t('Call Number'),
      'description' => t('Call Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'archiveLocation' => array(
      'title' => t('Loc. in Archive'),
      'description' => t('Loc. in Archive'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'repository' => array(
      'title' => t('Repository'),
      'description' => t('Repository'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'rights' => array(
      'title' => t('Rights'),
      'description' => t('Rights'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'extra' => array(
      'title' => t('Extra'),
      'description' => t('Extra'),
      'widget' => array(
        '#type' => 'textarea',
       ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
       ),
    ),

    'bookTitle' => array(
      'title' => t('Book Title'),
      'description' => t('Book Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'publicationTitle' => array(
      'title' => t('Publication'),
      'description' => t('Publication'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'issue' => array(
      'title' => t('Issue'),
      'description' => t('Issue'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 10,
       ),
    ),

    'seriesTitle' => array(
      'title' => t('Series Title'),
      'description' => t('Series Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'seriesText' => array(
      'title' => t('Series Text'),
      'description' => t('Series Text'),
      'widget' => array(
        '#type' => 'textarea',
        '#rows' => 3,
       ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
       ),
    ),

    'journalAbbreviation' => array(
      'title' => t('Journal Abbr'),
      'description' => t('Journal Abbr'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'DOI' => array(
      'title' => t('DOI'),
      'description' => t('DOI'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 100,
        '#size' => 100,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 100,
       ),
    ),

    'ISSN' => array(
      'title' => t('ISSN'),
      'description' => t('ISSN'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 24,
        '#size' => 24,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'section' => array(
      'title' => t('Section'),
      'description' => t('Section'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'thesisType' => array(
      'title' => t('Type'),
      'description' => t('Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'university' => array(
      'title' => t('University'),
      'description' => t('University'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'letterType' => array(
      'title' => t('Type'),
      'description' => t('Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'manuscriptType' => array(
      'title' => t('Type'),
      'description' => t('Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'interviewMedium' => array(
      'title' => t('Medium'),
      'description' => t('Medium'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'distributor' => array(
      'title' => t('Distributor'),
      'description' => t('Distributor'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'videoRecordingType' => array(
      'title' => t('Recording Type'),
      'description' => t('Recording Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'runningTime' => array(
      'title' => t('Running Time'),
      'description' => t('Running Time'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'artworkMedium' => array(
      'title' => t('Medium'),
      'description' => t('Medium'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'artworkSize' => array(
      'title' => t('Artwork Size'),
      'description' => t('Artwork Size'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'websiteTitle' => array(
      'title' => t('Website Title'),
      'description' => t('Website Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'websiteType' => array(
      'title' => t('Website Type'),
      'description' => t('Website Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'reportNumber' => array(
      'title' => t('Report Number'),
      'description' => t('Report Number'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 10,
        '#size' => 10,
       ),
      'data' => array(
        'type' => 'int',
        'not null' => FALSE,
       ),
    ),

    'reportType' => array(
      'title' => t('Report Type'),
      'description' => t('Report Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'institution' => array(
      'title' => t('Institution'),
      'description' => t('Institution'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'billNumber' => array(
      'title' => t('Bill Number'),
      'description' => t('Bill Number'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 24,
        '#size' => 24,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'code' => array(
      'title' => t('Code'),
      'description' => t('Code'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'codeVolume' => array(
      'title' => t('Code Volume'),
      'description' => t('Code Volume'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'codePages' => array(
      'title' => t('Code Pages'),
      'description' => t('Code Pages'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 10,
        '#size' => 10,
       ),
      'data' => array(
        'type' => 'int',
        'not null' => FALSE,
       ),
    ),

    'legislativeBody' => array(
      'title' => t('Legislative Body'),
      'description' => t('Legislative Body'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'session' => array(
      'title' => t('Session'),
      'description' => t('Session'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'history' => array(
      'title' => t('History'),
      'description' => t('History'),
      'widget' => array(
        '#type' => 'textarea',
        '#rows' => 3,
       ),
      'data' => array(
        'type' => 'text',
        'not null' => FALSE,
       ),
    ),

    'caseName' => array(
      'title' => t('Case Name'),
      'description' => t('Case Name'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'reporter' => array(
      'title' => t('Reporter'),
      'description' => t('Reporter'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'reporterVolume' => array(
      'title' => t('Reporter Volume'),
      'description' => t('Reporter Volume'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 24,
        '#size' => 24,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'court' => array(
      'title' => t('Court'),
      'description' => t('Court'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'firstPage' => array(
      'title' => t('First Page'),
      'description' => t('First Page'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'dateDecided' => array(
      'title' => t('Date Decided'),
      'description' => t('Date Decided'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'committee' => array(
      'title' => t('Committee'),
      'description' => t('Committee'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'documentNumber' => array(
      'title' => t('Document Number'),
      'description' => t('Document Number'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 10,
        '#size' => 10,
       ),
      'data' => array(
        'type' => 'int',
        'not null' => FALSE,
       ),
    ),

    'assignee' => array(
      'title' => t('Assignee'),
      'description' => t('Assignee'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'patentNumber' => array(
      'title' => t('Patent Number'),
      'description' => t('Patent Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'applicationNumber' => array(
      'title' => t('Application Number'),
      'description' => t('Application Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'priorityNumbers' => array(
      'title' => t('Priority Numbers'),
      'description' => t('Priority Numbers'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'issueDate' => array(
      'title' => t('Issue Date'),
      'description' => t('Issue Date'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'references' => array(
      'title' => t('References'),
      'description' => t('References'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'legalStatus' => array(
      'title' => t('Legal Status'),
      'description' => t('Legal Status'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'nameOfAct' => array(
      'title' => t('Name of Act'),
      'description' => t('Name of Act'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'codeNumber' => array(
      'title' => t('Code Number'),
      'description' => t('Code Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'publicLawNumber' => array(
      'title' => t('Public Law Number'),
      'description' => t('Public Law Number'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'dateEnacted' => array(
      'title' => t('Date Enacted'),
      'description' => t('Date Enacted'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'subject' => array(
      'title' => t('Subject'),
      'description' => t('Subject'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'mapType' => array(
      'title' => t('Type'),
      'description' => t('Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'scale' => array(
      'title' => t('Scale'),
      'description' => t('Scale'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'blogTitle' => array(
      'title' => t('Blog Title'),
      'description' => t('Blog Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'forumTitle' => array(
      'title' => t('Forum/Listserv Title'),
      'description' => t('Forum/Listserv Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'postType' => array(
      'title' => t('Post Type'),
      'description' => t('Post Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'audioRecordingType' => array(
      'title' => t('Recording Type'),
      'description' => t('Recording Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'label' => array(
      'title' => t('Label'),
      'description' => t('Label'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'presentationType' => array(
      'title' => t('Type'),
      'description' => t('Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'meetingName' => array(
      'title' => t('Meeting Name'),
      'description' => t('Meeting Name'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'studio' => array(
      'title' => t('Studio'),
      'description' => t('Studio'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'episodeNumber' => array(
      'title' => t('Episode Number'),
      'description' => t('Episode Number'),
      'widget' => array(
        '#type' => 'textfield',
        '#maxlength' => 24,
        '#size' => 24,
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 24,
       ),
    ),

    'network' => array(
      'title' => t('Network'),
      'description' => t('Network'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'audioFileType' => array(
      'title' => t('File Type'),
      'description' => t('File Type'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'version' => array(
      'title' => t('Version'),
      'description' => t('Version'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'system' => array(
      'title' => t('System'),
      'description' => t('System'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'company' => array(
      'title' => t('Company'),
      'description' => t('Company'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 255,
       ),
    ),

    'programmingLanguage' => array(
      'title' => t('Language'),
      'description' => t('Language'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 64,
       ),
    ),

    'proceedingsTitle' => array(
      'title' => t('Proceedings Title'),
      'description' => t('Proceedings Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'conferenceName' => array(
      'title' => t('Conference Name'),
      'description' => t('Conference Name'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'encyclopediaTitle' => array(
      'title' => t('Encyclopedia Title'),
      'description' => t('Encyclopedia Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

    'dictionaryTitle' => array(
      'title' => t('Dictionary Title'),
      'description' => t('Dictionary Title'),
      'widget' => array(
        '#type' => 'textfield',
       ),
      'data' => array(
        'type' => 'varchar',
        'not null' => FALSE,
        'length' => 128,
       ),
    ),

  );
}

function refcolab_creators_meta() {
  return array(
    'author' => array(
      'title' => t('Author'),
    ),

    'contributor'  => array(
      'title' => t('Contributor'),
    ),

    'editor' => array(
      'title' => t('Editor'),
    ),

    'translator' => array(
      'title' => t('Translator'),
    ),

    'seriesEditor' => array(
      'title' => t('Series Editor'),
    ),

    'interviewee'  => array(
      'title' => t('Interviewee'),
    ),

    'interviewer' => array(
      'title' => t('Interviewer'),
    ),

    'director' => array(
      'title' => t('Director'),
    ),

    'scriptwriter' => array(
      'title' => t('Scriptwriter'),
    ),

    'producer' => array(
      'title' => t('Producer'),
    ),

    'castMember' => array(
      'title' => t('Cast Member'),
    ),

    'sponsor' => array(
      'title' => t('Sponsor'),
    ),

    'counsel' => array(
      'title' => t('Counsel'),
    ),

    'inventor' => array(
      'title' => t('Inventor'),
    ),

    'attorneyAgent' => array(
      'title' => t('Attorney Agent'),
    ),

    'recipient' => array(
      'title' => t('Recipient'),
    ),

    'performer' => array(
      'title' => t('Performer'),
    ),

    'composer' => array(
      'title' => t('Composer'),
    ),

    'wordsBy' => array(
      'title' => t('Words By'),
    ),

    'cartographer' => array(
      'title' => t('Cartographer'),
    ),

    'programmer' => array(
      'title' => t('Programmer'),
    ),

    'artist' => array(
      'title' => t('Artist'),
    ),

    'commenter' => array(
      'title' => t('Commenter'),
    ),

    'presenter' => array(
      'title' => t('Presenter'),
    ),

    'guest' => array(
      'title' => t('Guest'),
    ),

    'podcaster' => array(
      'title' => t('Podcaster'),
    ),

    'reviewedAuthor' => array(
      'title' => t('Reviewed Author'),
    ),
  );
}
