<?php

/**
 * @file
 * Page callbacks for adding, editing, deleting, and overview of user collections. 
 */

/**
 * Page callback -- display the collections overview page for the 
 * given user account. 
 *
 * @param $account
 *   The user account to display the collections for. 
 */
function refcolab_collections_overview($account) {
  $collection = array('cid' => 0, 'uid' => $account->uid, 'parent' => 0, 'name' => '', 'description' => '', 'weight' => 0);
  $return = drupal_get_form('refcolab_collections_form', $collection).
    drupal_get_form('refcolab_collections_overview_form', $account);
  return $return;  
}

/**
 * Form builder callback for the collections form.
 * 
 * @see refcolab_collections_form_submit().
 */
function refcolab_collections_form(&$form_state, $collection) {
  $collection = (object)$collection;
  $form['#tree'] = TRUE;
  $form['collection'] = array(
    '#type' => 'fieldset',
    '#title' => $collection->cid ? t('Edit collection') : t('Add a new collection'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['collection']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $collection->name, 
    '#size' => 20,
    //'#description' => t('The name of the collection.'),
  );
  $form['collection']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $collection->description,   
    '#size' => 30,
    //'#description' => t('A brief description about what is this collection intended for.'),
  );
  
  $tree = refcolab_collections_get_tree($collection->uid);

  // A collection can't be the child of itself, nor of its children.
  $exclude = array();
  if ($collection->cid){
    $children = refcolab_collections_get_tree($collection->uid, $collection->cid);
    foreach ($children as $child) {
      $exclude[] = $child->cid;
    }
    $exclude[] = $collection->cid;
  }
  
  if (count($tree)){
    $form['collection']['parent'] = refcolab_collection_select($tree, t('Parent collection'), $collection->parent, $exclude);
  }  

  $form['collection']['save'] = array(
    '#type' => 'submit',
    '#value' => $collection->cid ? t('Save') : t('Add new'),
  );
  $form['collection']['uid'] = array('#type' => 'value', '#value' => $collection->uid);
  $form['collection']['cid'] = array('#type' => 'value', '#value' => $collection->cid);
  
  return $form;
}

/**
 * Submit callback for the collection form.
 */
function refcolab_collections_form_submit($form, &$form_state) {
  refcolab_collections_save($form_state['values']['collection']);
  if ($form_state['values']['collection']['cid']) {
    drupal_set_message(t('The collection has been saved.'));
    $form_state['redirect'] = 'admin/content/collections/'. $form_state['values']['collection']['uid'];
  }
  else {
    drupal_set_message(t('The collection has been created.'));
  }
}

/**
 * Form builder callback for the collections form.
 * 
 * @param $form_state
 *   The form state array.
 * @param $account
 *   The given user account object.
 * 
 * @see refcolab_collections_overview_form_submit().
 */
function refcolab_collections_overview_form(&$form_state, $account) {
  $form['#tree'] = TRUE; 
  $form['uid'] = array('#type' => 'value', '#value' => $account->uid);
   
  // Get the user collections tree. 
  $collections = refcolab_collections_get_tree($account->uid);
   
  foreach ($collections as $item) {
    $form[$item->cid]['#collection'] = $item;
    $form[$item->cid]['cid'] = array('#type' => 'hidden', '#default_value' => $item->cid);        
    $form[$item->cid]['name'] = array('#value' => check_plain($item->name), '#description' => $item->description);
    $form[$item->cid]['weight'] = array('#type' => 'weight', '#delta' => 20, '#default_value' => $item->weight);
    $form[$item->cid]['parent'] = array('#type' => 'hidden', '#default_value' => $item->parent);    
    $form[$item->cid]['edit'] = array('#value' => l(t('edit'), "admin/content/collections/collection/$item->cid"));
    if (!$item->is_parent) {
      $form[$item->cid]['delete'] = array('#value' => l(t('delete'), "admin/content/collections/collection/$item->cid/delete"));
    }
  }
  
  $form['save'] = array('#type' => 'submit', '#value' => t('Save'), '#weight' => 10);  
  
  return $form;
}

/**
 * Submit handler for the collections overview form. 
 */
function refcolab_collections_overview_form_submit($form, $form_state) {
  foreach ($form_state['values'] as $cid => $item) {
    db_query("UPDATE {refcolab_collections} SET weight = %d, parent = %d WHERE cid = %d", $item['weight'], $item['parent'], $cid);
  }
  drupal_set_message(t('The collection tree has been saved.'));
}

/**
 * Theme the collection overview form.
 *
 * @ingroup themeable
 */
function theme_refcolab_collections_overview_form($form) {
  drupal_add_tabledrag('collection-overview', 'match', 'parent', 'collection-overview-parent', 'collection-overview-parent', 'collection-overview-cid');
  drupal_add_tabledrag('collection-overview', 'order', 'sibling', 'collection-overview-weight');

  $rows = array();
  foreach(element_children($form) as $key) {
    if (isset($form[$key]['#collection'])) {
      
      // Add special classes to be used with table drag behavior.
      $form[$key]['weight']['#attributes']['class'] = 'collection-overview-weight';
      $form[$key]['parent']['#attributes']['class'] = 'collection-overview-parent';
      $form[$key]['cid']['#attributes']['class'] = 'collection-overview-cid';
      
      // Build the row rendering the form elements in the columns. 
      $row = array(
        theme('indentation', $form[$key]['#collection']->depth). 
        drupal_render($form[$key]['name']).
        '<span class="description">'. check_plain($form[$key]['name']['#description']). '</span>',
        
        drupal_render($form[$key]['cid']).
        drupal_render($form[$key]['parent']),
        
        drupal_render($form[$key]['weight']),
        
        drupal_render($form[$key]['edit']). ' '.
        drupal_render($form[$key]['delete']),
      );
      $rows[] = array('data' => $row, 'class' => 'draggable'); 
    }
  }

  $header = array(t('Collections'), '', t('Weight'), t('Operations'));
  $output = ''; 
  $output .= theme('table', $header, $rows, array('id' => 'collection-overview'));
  $output .= drupal_render($form);
  
  return $output;
}

function theme_refcolab_collections_form($form){
  drupal_add_css(drupal_get_path('module', 'refcolab_collections') .'/refcolab_collections.css', 'module', 'all', FALSE);
  
  $form['collection']['name']['#prefix'] = '<div class="refcolab-collection-left">';
  $form['collection']['name']['#suffix'] = '</div>';
  
  $form['collection']['description']['#prefix'] = '<div class="refcolab-collection-right">';
  $form['collection']['description']['#suffix'] =  '</div>';

  $form['collection']['parent']['#prefix'] = '<div class="clear-block clear">';
  $form['collection']['parent']['#suffix'] =  '</div>';
  
  return drupal_render($form);
}

function refcolab_collections_delete_confirm(&$form_state, $collection){
  $collection = (object)$collection;
  $form['cid'] = array('#type' => 'hidden', '#value' => $collection->cid);
  $form['uid'] = array('#type' => 'hidden', '#value' => $collection->uid);
  $form['#submit'] = array('delete_submit');
  return confirm_form($form, 
    t('Are you sure you want to remove %name from the collections tree?', array('%name' => $collection->name)), 
    'admin/content/collections/collection/%refcolab_collection', 
    t('This action cannot be undone.'), 
    t('Delete'), 
    t('Cancel')
  );
  
}

function delete_submit($form, &$form_state) {
  db_query("DELETE FROM {refcolab_collections} WHERE cid = %d", $form_state['values']['cid']);
  //print_r($form_state['values']['uid']);
  $form_state['redirect'] = 'admin/content/collections/'.$form_state['values']['uid'];
  drupal_set_message('The collection has been deleted'); 
  return;
}